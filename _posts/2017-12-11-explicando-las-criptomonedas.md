---
layout: post
title: "Recopilación general de información sobre Criptomonedas"
date: 2017-12-11 00:03:37 -06:00
type: post
published: true
status: publish
categories: []
tags: [tutorial]
author: "Energy1011"
image: "criptomonedas.jpg"
---
<div align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Gc2en3nHxA4?ecver=2" frameborder="0" gesture="media" allowfullscreen></iframe>
</div>
[Las criptomonedas](https://es.wikipedia.org/wiki/Criptomoneda) han llegado sobre una idea innovadora y pretenden quedarse para funcionar por mucho tiempo, esto __No es tema nuevo__, el [Bitcoin](https://es.wikipedia.org/wiki/Bitcoin) por ejemplo, que implementa un protocolo open source, fue creado desde el 2009 por [Satoshi Nakamoto](https://es.wikipedia.org/wiki/Satoshi_Nakamoto), quien/quines decidieron guardar el anonimato y no atribuirse la creación del protocolo y moneda bitcoin de inmediato.

> Artículo de wikipedia: En el año 2015, Satoshi Nakamoto fue el ganador del Premio a la Innovación de The Economist en su categoría especial «Sin Límites» por su invención, «que podría alterar radicalmente el sistema financiero internacional [...] Eliminaría la necesidad de terceros de confianza como los bancos centrales en la transmisión de dinero, podría reducir el coste de tarjetas de crédito y otras tarifas en las transacciones, y atraer a quienes defienden la privacidad»

> Artículo de wikipedia: La propuesta de nominación al Premio Nobel de Economía de 2016 para Satoshi Nakamoto fue rechazada desde la oficina de prensa de la Real Academia de las Ciencias de Suecia porque no se otorga si el galardonado es «anónimo o ha fallecido».

 Si hubiésemos comprado 1 bitcoin en sus inicios, cuando una sola unidad costaba apenas unos pocos dólares, __sí, una sola unidad__, al momento de escribir este post, un solo bitcoin cuesta nada más ni nada menos que __$19,285.07 USD__, así que imaginen el arrepentimiento para muchos, de no haber adquirido tan solo 1 BTC en aquel entonces.

> __Investigue lo suficiente sobre seguridad en criptomonedas y construyan su propio criterio__, para evitar ser presa fácil de los fraudes.

Le recomiendo leer el siguiente articulo completo sobre [bitcoin en wikipedia](https://es.wikipedia.org/wiki/Bitcoin) y en lo siguiente de este post les comparto un resumen que he elaborado, y lo necesario para saber sobre las criptomonedas.

Enlaces de referencia al final del post.

### RESUMEN Conocimiento básico de las criptomonedas
Las criptomonedas son un medio digital de intercambio, todas ellas se manejan virtualmente (por medio de tecnología) y este es el medio donde las podemos utilizar, son monedas virtuales, no son físicas si las comparamos con las monedas convencionales que están hechas de algun tipo de mental, y las criptomonedas cada día son más aceptadas como [aquí lo mencionan](http://www.diariobitcoin.com/index.php/2017/04/05/bitcoin-sera-aceptado-en-260-000-tiendas-de-japon-este-verano/) para el comercio.

Las criptomonedas son datos/información que tiene valor global por capitalización incluso el respaldo puede ser oro, petróleo, diamantes etc, como se pretende con la criptomoneda recientemente planteada el [Petro en Venezuela](https://es.wikipedia.org/wiki/Petro_Moneda). En este momento el bitcoin está capitalizado en __$323,001,252,140.17 USD__ como podemos verificar [aquí](https://www.coingecko.com/es).

### ¿ Quién y cómo es controlada la contabilidad de las criptomonedas ?
Estas criptomonedas son contabilizadas y controladas por medio de redes de computadoras, la llamada [Red Blockchain](https://www.blockchain.com/) (para el bitcoin), la diferencia principal de ellas __al dinero moneda de metal de tu país__ es que __las criptomonedas son descentralizadas y no dependen de un banco para ser intercambiadas (van directamente del comprador al vendedor)__ directamente entre wallets (carteras) virtuales, donde se guardan las criptomonedas "como si fuese una cuenta de banco".

Las criptomonedas y sus transferencias y movimientos existen y se mueven como datos protegidos en las redes de computadoras ([Red Blockchain](https://www.blockchain.com/) en el caso del bitcoin y otras criptomonedas con bifurcaciones basadas en este sistema), la red de bloques es código [Open source](https://es.wikipedia.org/wiki/C%C3%B3digo_abierto), y las criptomonedas están protegidas por algoritmos de encriptación, por eso su nombre (cripto-monedas) por estar encriptadas.

[La Red Blockchain](https://www.blockchain.com/), es una red conformada con el propósito de operar las transacciones del bitcoin, esta red es conformada y controlada por cualquier persona que ha querido unirse, en teoría podemos unirnos/contribuir a esta red blockchain, pero en práctica unirnos individualmente sin gran poder de cómputo, en inversión, esto no parece ser algo rentable, dado a que la red ya está conformada por personas que han invertido en gran poder de cómputo, y un ordenador casero consumirá más valor en electricidad que lo que pudieses ganar operando en la red de manera independiente. Las personas que se unen a la red blockchain son los llamados mineros, se les llaman así porque trabajan en mantener la red y las transacciones funcionando y reciben una pequeña comisión por ello, por las transacciones realizadas en sus grandes granjas de procesamiento. Existen otras maneras de unirse a la minería en el blockchain por medio de [Cloud Mining](https://en.wikipedia.org/wiki/Cloud_mining).

> Block Chains, Cadenas de bloques: En el campo de las criptomonedas la cadena de bloques se usa como notario público no modificable de todo el sistema de transacciones a fin de evitar el problema de que una moneda se pueda gastar dos veces. Por ejemplo es usada en Bitcoin, Ethereum, Dogecoin y Litecoin, aunque cada una con sus particularidades .

### Distintas criptomonedas
Hoy en día existen muchas criptomonedas que operan más o menos igual al bitcoin que ahora es la madre de todas. Aun así hay que echar un vistazo a las otras criptomonedas que se han ido capitalizando, y podrian ser buena oportunidad:

- [Ethereum](https://www.ethereum.org/)
- [Ripple](https://ripple.com/)
- [LiteCoin](https://litecoin.org/)

"He decidio adquirir Ethereum y Ripple, tengo la creencia/esperanza de que pueden llegar a capitalizarse lo suficiente para obtener ganancia de ello y que el fenómeno pueda en cierta manera repetirse como sucedió con el bitcoin."

### Plataformas de intercambio, trading online

Existen múltiples plataformas de intercambio, son conocidas tambien como casas de cambio, ahi existe la compra y venta de criptomonedas, [Aqui un listado de los top brookers](https://www.top-brokers.org/es/top/criptomonedas/bitcoin-comprar/?gclid=EAIaIQobChMIzp7ogNeU2AIV1rbACh3SQwiiEAAYASAAEgLQxfD_BwE)

Recomendación: si piensan usar una de estas plataformas online, siempre es recomendable usar plataformas que lleven más tiempo funcionando y que operen con medidas de seguridad más controladas, me refiero a que por ejemplo, implementen login de cuentas a las wallets con autenticación de dos factores y que posean mapeo de IP de logeo recurrente, etc.

### Alternativas ¿ Cómo aseguro mis criptomonedas ?
Conforme las criptomonedas se van haciendo más utilizadas, se han creado incluso mecanismos y hardware de [Cold storage](https://en.bitcoin.it/wiki/Cold_storage), un ejemplo es [Ledger nano](https://www.ledgerwallet.com/), para controlar de manera más 'fisica' y offline nuestras reservas más grandes de criptomonedas, [aquí](https://en.bitcoin.it/wiki/Cold_storage) un enlace de ayuda.

>Cold storage in the context of Bitcoin refers to keeping a reserve of Bitcoins offline. This is often a necessary security precaution, especially dealing with large amounts of Bitcoin.

Si no quiere arriesgar con ninguna plataforma sus criptomonedas en carteras online, entonces puede crear su propia cartera privada y resguardar sus criptomonedas a manera offline (esto sin duda, sería lo más recomendable, para guardar grandes depósitos), ya que si roban las wallets de una plataforma online, sus criptomonedas no estarán ahí, sino en su computadora u otro medio de almacenamiento de [Cold storage](https://en.bitcoin.it/wiki/Hardware_wallet) o [Hardware wallet](https://en.bitcoin.it/wiki/Hardware_wallet).

### ¿ Las criptomonedas son peligrosas en el sentido de que se puede perder mucho dinero ?
Tener en cuenta que las criptomonedas son extremadamente volátiles, pueden aumentar y disminuir su valor bruscamente, puede revisar la grafica-historial [Aquí del Bitcon](https://www.coingecko.com/es/tabla_de_precios/bitcoin/usd#panel) o [Aquí del Ethereum](https://www.coingecko.com/es/tabla_de_precios/ethereum/usd#panel) o cualquier otra moneda del listado.

### Cierre y puntos importantes
El ecosistema virtual de las criptomonedas puede ser utilizado para fraudes sobre sistemas piramidales o ataques informáticos, siempre hay que realizar buenas prácticas de seguridad para mantenernos a salvo y estar siempre bien informados.

Las criptomonedas son bastante volátiles, pueden aumentar y disminuir su valor de un momento a otro, y existe la tentación de ganar mucho en ellas, ya que si las adquirimos a cierto precio este valor puede verse duplicado o triplicado en lapsos de tiempo muy cortos [ver gráficas de historial de valores](https://www.coingecko.com/es/tabla_de_precios/bitcoin/usd#panel).

Las criptomonedas tienen la ventaja de ofrecer mecanismos de anonimato por ser descentralizadas y no depender de bancarios, es por eso, que por ejemplo ciberdelincuentes hacen uso de wallets no rastreables para recibir pagos de acciones fraudulentas como los [ransomware](https://es.wikipedia.org/wiki/Ransomware).

Son virtuales y se mueven en la red, una vez realizada una transacción ya no hay marcha atrás, y no tenemos manera de reclamar por el robo de nuestras llaves privadas o carteras, o por equivocación de traspaso a una cuenta erronea o de otro tipo de criptomoneda.

La dirección de un wallet puede lucir algo así: __34jk4f31jJKLLj432j3424lakdka423__ es por eso que se usan [QR](https://es.wikipedia.org/wiki/C%C3%B3digo_QR) para evitar el error al escribirlas manualmente.

Existen plataformas para el almacenamiento de criptomonedas en wallets, pero tenemos la libertad de transferirlas a otras wallets incluso privadas a manera offline en nuestras computadoras y otros dispositivos de cold storage, para no arriesgarse a que la plataforma sufra robos de wallets o los dueños se desaparezcan.

Los inversionistas de valor tradicionales hablan de la dificultad para predecir los valores de las criptomonedas (a manera tradicional), para utilizarlas como instrumento de inversión, ya que la naturaleza de las criptomonedas complica calcular el valor intrínseco de las mismas.

Por el momento es todo, espero esta recopilación de información sea útil.

- [https://es.wikipedia.org/wiki/Criptomoneda](https://es.wikipedia.org/wiki/Criptomoneda)
- [https://es.wikipedia.org/wiki/Bitcoin](https://es.wikipedia.org/wiki/Bitcoin)
- [https://es.wikipedia.org/wiki/Satoshi_Nakamoto](https://es.wikipedia.org/wiki/Satoshi_Nakamoto)
- [http://www.diariobitcoin.com/index.php/2017/04/05/bitcoin-sera-aceptado-en-260-000-tiendas-de-japon-este-verano/](http://www.diariobitcoin.com/index.php/2017/04/05/bitcoin-sera-aceptado-en-260-000-tiendas-de-japon-este-verano/)
- [https://es.wikipedia.org/wiki/Petro_Moneda](https://es.wikipedia.org/wiki/Petro_Moneda)


