---
layout: post
title: 'Opinión personal sobre el uso de redes sociales cerradas para la difusión del software libre'
date: 2017-09-04 01:00:07 -06:00
type: post
published: true
status: publish
categories: [linux]
tags: [linux, bash, apt]
meta:
author: "Energy1011"
image: "logos-colash.png"
---

Personalmente **NO** estoy de acuerdo con la filosofía de servicios de redes cerradas como facebook, g+, etc. Sin embargo **Sí** estoy de acuerdo en lo que argumentan algunos difusores de [Software libre](https://www.gnu.org/philosophy/free-sw.es.html):

> Para poder llegar a más personas, dar a conocer y compartir nuestra filosofía libre habrá que llegar hasta donde la mayoría de gente se encuentre actualmente, donde el impacto de difusión sea mayor, nuestra batalla de difusión está ahí.

En lo que **NO** estoy de acuerdo es que dejemos de difundir y usar alternativas libres por creer que es una lucha perdida, ya que muchas personas se desaniman comparando la cantidad de usuarios entre las redes sociales cerradas y las libres, lo mismo pasa con muchos otros servicios.

Listado de alternativas a varios modos de comunicación y redes sociales puedes ser las siguientes:
- A Whatsapp tenemos [Telegram](https://telegram.org/).
- A Facebook tenemos [GNU-Social](https://gnu.io/social/try/), [Diaspora](https://www.joindiaspora.com/).
- A Twitter tenemos [Mastodon](https://mastodon.social/about).
- A Youtube tenemos [MediaGoblin](https://mediagoblin.org/).

## Sobre sistemas operativos libres
Por otra parte los sistemas operativos me parecen prioridad para aumentar la cuota de uso de alternativas libres y haré una comparativa en el contexto de los sistemas operativos: Si a GNU/Linux lo hubieran considerado una lucha perdida en sus inicios (cuando tenía pocos usuarios) no tendríamos la oportunidad de disfrutarlo actualmente, además pensemos en la enorme diferencia de cantidad de gente que hoy en día disfruta usando GNU y Linux como proyectos juntos y separados, sin duda la diferencia y avance es enorme.

GNU/Linux hoy en día cuenta con una cantidad impresionante de distribuciones aunque no todas son [100% distribuciones libres](https://www.gnu.org/distros/) estas han tenido cambios notables:
- Se están desarrollando más videojuegos desde la llegada de steam a GNU/Linux (si bien todo lo de steam no es 100 % libre esto trae consigo más usuarios).
- Mejoras importantes en APIs open para gráficas compatibles con GNU/LINUX como [Vulkan](https://es.wikipedia.org/wiki/Vulkan).
- Las distribuciones cada día se acercan más a ofrecer mejor experiencia de uso a usuarios domésticos, además hoy existe variedad de entornos gráficos distintos para GNU/Linux, "vaya que son bonitos", son más user-friendly de lo que eran hace años, tambien tenemos [Centros de software](https://es.wikipedia.org/wiki/Centro_de_software_de_Ubuntu) (tipo tiendas virtuales para adquirir software) que ayudan a instalar programas a los usuarios de GNU/Linux con un solo click.
- Se sigue desarrollando y ampliando la compatibilidad de arquitecturas para las distintas CPU (el proyecto [Debian/Ports](https://www.debian.org/ports/index.es.html) destaca y se puede usar de ejemplo por algo su slogan "The Universal operating system").
- Están madurando distintos tipos de aplicaciones y proyectos para cubrir la gran demanda de variedad de software necesario.

Si bien, algunas de estás razones pueden no ser 100 % libres, al menos se acercan un poco más a la libertad.

Para mi el **software libre** tiene una gran ventaja sobre el **software privativo** y es la cantidad de contribuciones de desarrollo y esto me recuerda a los dichos:
> - Dos cabezas piensan mejor que una.
- Divide el trabajo y vencerás.

Con lo anterior quiero decir que multitud de programadores de software libre pueden llegar a aportar y contribuir mucho más que lo que puede costear una empresa de software privativo en programadores. 

>[(Recordemos que no es lo mismo libre que gratis)](https://blog.desdelinux.net/cual-es-la-diferencia-entre-el-software-libre-y-el-software-gratuito/).

En una era donde ya existen [CVS libres](https://es.wikipedia.org/wiki/CVS) como git y plataformas que facilitan el desarrollo del software libre (bitbucket, gitlab, etc) donde son cada vez más usadas, accesibles y la programación se hace al alcance de todos, la velocidad de desarrollo y calidad del software libre se incrementa a pasos agigantados. Es por ello que empresas como microsoft actualmente han optado por liberar código (core .net, visual studio, powershell) para poder igualar estos pasos agigantados cuando nunca en el pasado de sus proyectos fue su intención hacerlo y se veia a GNU/Linux como un cancér. 

[Ballmer: «Linux ya no es un cáncer, es un rival verdadero de Windows»](https://www.linuxadictos.com/ballmer-linux-ya-no-es-un-cancer-es-un-rival-verdadero-de-windows.html)

## Conclusión y frase final
Recordemos que hay demasiada gente que no tiene ni la más mínima idea de que es el software libre, ha esa gente podriamos invitarla a conocer sobre que la __Libertad también importa dentro de la informática y juega un papel importante__. 

Si bien a muchos no nos es posible usar puramente y unicamente software 100% libre, al menos en mi opinion comenzar a usar lo más que se pueda:
> A veces no se puede pasar de negro a blanco sin tener que presenciar los grises, pero creo que mientras más cerca nos encontremos de la libertad y el control de nuestra informática mejor.

## Invito a todos
Invito a seguir difundiendo/apoyando/colaborando con todas las aplicaciones libres y las redes como [GNU-Social](https://gnu.io/social/try/), [Mastodon](https://mastodon.social/about) o [Diaspora](https://www.joindiaspora.com/) que buscan ofrecer un control y una libertad de nuestra información distinta.

Nos queda mucho trabajo por hacer y esto lleva tiempo, existen multitud de maneras para contribuir con el software libre y su movimiento:
- Difundiendo por cualquier medio.
- Donando.
- Programando.
- Escribiendo material de documentación.
- Utilizando y probando software libre.
- Realizando feedbacks y aportando ideas.
- Traduciendo manuales y software.

Unete, la información necesaria la podemos encontrar en:
- <https://www.gnu.org/home.es.html>
- <https://www.fsf.org/es>

Puedes unirte también a grupos de difusión de hispano hablantes, por ejemplo en telegram:
- <https://t.me/maratonlinuxero>
- <https://t.me/GNULinuxGrupo>
- <https://t.me/podcastlinux>
- <https://t.me/gnulinuxmx>

También aquí tengo mi lista de sitios bookmarks interesantes:
[Mis bookmarks](https://energy1011.gitlab.io/monsterpenguin/bookmarks/)

Este blog es libre, puedes clonarlo/modificarlo/distribuirlo desde: <https://gitlab.com/Energy1011/monsterpenguin>
Los screeencast de este blog son subidos a archive.org: <https://archive.org/details/@monster_penguin>
