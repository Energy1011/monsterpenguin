---
layout: post
title: "OnlyOffice otra alternativa de ofimática"
date: 2018-08-30 19:37:28 -06:00
type: post
published: true
status: publish
categories: [onlyoffice]
tags: [onlyoffice]
author: "Energy1011"
image: "logo-onlyoffice.png"
---

[OnlyOffice](https://www.onlyoffice.com/) es una _all-in-one_ suite de [Ofimática](https://es.wikipedia.org/wiki/Ofim%C3%A1tica) con licencia [AGPLv3](https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License) que he instalado recientemente y probado en mi sistema [GNU/Linux](https://es.wikipedia.org/wiki/GNU/Linux) en unos pocos minutos de manera __Gratuita en su edición [Desktop Editors](https://www.onlyoffice.com/apps.aspx)__. __OnlyOffice__ es multiplataforma disponible para varios sistemas operativos, incluso se puede descargar de (google play para android y app store) con algunas limitaciones por el momento, pero tal parece que se encuentra en desarrollo constante.

Esta suite de ofimática se apega a los formatos [Libres](https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto) que son una alternativa a los formatos de [Software Privativo](https://es.wikipedia.org/wiki/Software_propietario) que va a mucho sobre la importancia de ["Software libre y Educación"](https://www.gnu.org/education/education.es.html)

Lo que me gusta de __OnlyOffice__ es que puede crear y abrir archivos del tipo [OpenDocument](http://opendocument.xml.org/overview) este tipo de archivos son los que utilizamos con [Libre Office](https://www.libreoffice.org/) y son [Formatos Abiertos](https://es.m.wikipedia.org/wiki/Formato_abierto), a diferencia de los [Formatos Privativos](https://es.wikipedia.org/wiki/Software_propietario) en los que no podemos descubrir cómo funcionan exactamente por dentro por ser cerrados, aún así son ampliamente utilizados a confianza, [¿Puede confiar en su ordenador?](https://www.gnu.org/philosophy/can-you-trust.es.html)

## Características de OnlyOffice
Es posible exportar archivos [OpenDocument](https://www.fsf.org/es/documentos/opendocument) __(.ods, .odt, .odp, .odg)__ o __.pdf__ desde sin necesidad de pagar por una licencia.

__OnlyOffice__ en su edición __(Desktop Editors)__ tiene software para manejar los siguientes tipos de archivos:
- [Documentos de texto](https://es.wikipedia.org/wiki/LibreOffice_Writer)
- [Hojas de cálculo](https://es.wikipedia.org/wiki/Hoja_de_c%C3%A1lculo)
- [Archivos de presentaciones](https://es.wikipedia.org/wiki/Programa_de_presentaci%C3%B3n)
<p style="text-align:justify;"><img class=" size-full aligncenter" src="{{ site.baseurl }}/assets/onlyoffice1.jpg" alt="OnlyOffice" width="600" height="400" /></p>
<p style="text-align:justify;"><img class=" size-full aligncenter" src="{{ site.baseurl }}/assets/onlyoffice2.jpg" alt="OnlyOficce" width="600" height="400" /></p>
<p style="text-align:justify;"><img class=" size-full aligncenter" src="{{ site.baseurl }}/assets/onlyoffice3.jpg" alt="OnlyOficce" width="600" height="400" /></p>

En las imágenes anteriores podemos ver la interfaz de __OnlyOffice Desktop Editors__, para los usuarios a quienes por ejemplo no les agrada del todo la interfaz de las aplicaciones de [Libre Office](https://www.libreoffice.org/) porque les parece anticuada (pasada de moda), entonces OnlyOffice podría ser una buena opción para seguir trabajando con formatos libres de [OpenDocument](https://www.fsf.org/es/documentos/opendocument) ya que tiene una estética visual más actualizada "por decirlo de una manera"

>En lo personal la estética visual no la valoro más que a la libertad o la funcionalidad que pueda tener un programa, pero comprendo que existe diversidad de gustos y objetivos.

  __Para finalizar y como motivo principal de esta publicación__ quisiera recomendarle conocer los formatos [OpenDocument](https://www.fsf.org/es/documentos/opendocument) o el software [Libre Office](https://www.libreoffice.org/) y [OnlyOffice](https://www.onlyoffice.com/) en su edición __(Desktop Editors)__ , ya que estos los podemos usar de manera libre y gratuita además de que no tenemos que pagar en licencias.


Enlaces:
- [OnlyOffice](https://www.onlyoffice.com/)
- ["Software libre y la Educación"](https://www.gnu.org/education/education.es.html)
- [OpenDocument](https://www.fsf.org/es/documentos/opendocument)
- [GNU/Linux](https://es.wikipedia.org/wiki/GNU/Linux)
- [Software libre](https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto)
- [Software propietario](https://es.wikipedia.org/wiki/Software_propietario)
- [Documentos de texto](https://es.wikipedia.org/wiki/LibreOffice_Writer)
- [Hojas de cálculo](https://es.wikipedia.org/wiki/Hoja_de_c%C3%A1lculo)
- [Archivos de presentaciones](https://es.wikipedia.org/wiki/Programa_de_presentaci%C3%B3n)
