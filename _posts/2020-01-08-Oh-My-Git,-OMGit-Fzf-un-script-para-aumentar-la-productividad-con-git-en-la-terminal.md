---
layout: post
title: "Oh My Git, OMGit-Fzf un script para aumentar la productividad con Git en la terminal"
date: 2020-01-8 02:00:00 -06:00
type: post
published: true
status: publish
categories: [git, script, productividad]
tags: [git, script, productividad]
author: ""
meta:
image: "omgit-fzf.png"
---

De vuelta con las publicaciones en mi blog, después de mucho tiempo metido en otros proyectos, el día de hoy les traigo un aporte a manera de script que he programado llamado **"Oh My Git" [OMGit-fzf](https://gitlab.com/Energy1011/omgit)**.

Para los que no conocen el proyecto [Fzf](https://github.com/junegunn/fzf), primero es necesario clarificar qué es Fzf y para qué sirve, ya que OMGit-fzf hace uso de este comando.

Fzf es un **command-line fuzzy finder que me parece sorprendente en velocidad y eficiencia.** En la siguiente sección explicaré primero qué es Fzf, y si ya conoce este potente programa puede saltar tranquilamente los apartados de esta publicación donde explico Fzf.

## ¿ Qué es Fzf ?

Fzf es un buscador 'fuzzy' para la terminal de propósito general, en su repo viene la siguiente descripción del proyecto:

> 'fzf is a general-purpose command-line fuzzy finder.'

La descripción anterior puede sonar engorrosa o difícil de entender a primera vista. Trataré de explicarlo sin meternos en tanto lío: Fzf es un buscador y selector de propósito general, con Fzf podemos realizar búsquedas en la terminal en tiempo real, ya que al llamar a Fzf este nos permite ir escribiendo nuestra búsqueda, y con cada teclazo, Fzf nos va mostrando posibles resultados de nuestra búsqueda a manera de una lista seleccionable, autocompletando y señalando cada una de las palabras o letras que vamos escribiendo como resultado.

Solo a manera de referencia, Fzf puede realizar entre otras cosas algo muy parecido a una búsqueda de archivos en un IDE, y para todos los usuarios de IDE's o editores de texto, como pueden ser vim, sublime text, atom, visual code, etc. Este tipo de búsqueda búsqueda 'fuzzy' suena familiar, la realizamos cuando en un proyecto grande queremos buscar y abrir archivos rápidamente con un shortcut que en la mayoría de estos editores y plugins manejan por default: ctrl+p o ctrl+shift+p

### ¿ Qué podemos buscar con Fzf ?

Este potente buscador no se limita solo a buscar archivos o carpetas, **Fzf puede buscar en cualquier cosa que le pasemos como entrada estándar**, por eso cuando los creadores de Fzf han dicho que es un buscador de propósito 'general' se refieren a esto, con **Fzf podemos buscar cualquier cosa que reciba como entrada estándar**: archivos, carpetas, palabras en un listado o texto, etc.

Quiero mencionar que he llegado a conocer este comando gracias a un plugin del editor Vim llamado [vim.fzf](https://github.com/junegunn/fzf.vim) en el que precisamente hace años necesitaba un medio que me permitiera buscar y abrir rápidamente archivos dentro de vim en proyectos de software grandes, en los que encontrar archivos para modificarlos se puede volver un infierno y una pérdida de tiempo tremenda.

> Fzf es utilizado no solo llamándolo desde vim, ya que cuando instalamos Fzf lo hacemos a nivel sistema operativo.

### ¿ Cómo podemos decirle a Fzf entre qué cosas buscar ?

Fzf trabaja de la misma manera que muchos otros comandos en sistemas \*nix-like, quiere decir que este puede recibir datos en su entrada estándar, y va a trabajar las búsquedas con esta entrada. Para los que usamos la terminales \*nix-like o lenguajes de programación a menudo, conocemos un concepto llamado entrada y salida estándar usando pipes entre comandos.

> El símbolo **'\|'** pipe en la terminal nos permite conectar comandos mediante sus entradas y salidas estándar, y así es cómo Fzf funciona.

No hay de qué preocuparse si no se comprende bien lo anterior, dejo una lista de conceptos que pueden ser de ayuda para aclarar:

- Pipes en GNU/linux
- Entrada estándar
- Salida estándar

**> Al final agrego enlaces de referencia**

La salida estándar de Fzf será finalmente lo que seleccionemos dentro de lo que nos ha listado Fzf que coincida con nuestra búsqueda.

### Mi necesidad por crear OMGit-fzf

Bien, ahora que sabemos cómo trabaja Fzf, puedo hablar de cómo surgió mi necesidad de ir más rápido con Git en la terminal y me hice un script bastante simple y de pocas líneas de código para hacer uso de Fzf con Git al que llamé **OMGit-fzf**

Cuando se trabaja con proyectos de software pequeños en los que vamos agregando pocos archivos de código usar git desde la terminal es rápido, sobre todo cuando hacemos uso de la tecla **TAB** para por ejemplo hacer un

```bash
$ git add /src/carpetaX/archivo.py
```

Escribir el comando anterior no nos llevaría mucho tiempo. Bien ahora imaginemos que trabajamos en un proyecto grande donde de manera automatizada se crean archivos con templates de código, modelos, clases, etc. en distintos directorios. Con lo anterior estamos entonces frente a una pérdida de tiempo inminente y para poderlo resolver tenemos la opcion de utilizar algún GUI para git o un plugin dentro de nuestro editor que nos permita usar git de una manera más "rápida", e ir con el mouse y a botonazos agregando todos estos archivos. No está mal, pero, **¿ Qué pasa cuando tenemos cincuenta o incluso cientos de archivos en paths distintos a lo largo de la estructura del proyecto ?** En este caso incluso ir con el mouse dando clic o seleccionando todos los archivos nos puede llevar también bastante tiempo.

> Personalmente me considero un adicto a la velocidad en la terminal, busco siempre mantener mi atención y mi workflow en la terminal, mientras menos salga de la terminal y mientras menos despegue las manos del teclado, soy más veloz y productivo.

__Entonces un día que estaba frente a un problema como estos de un listado de cientos de archivos creados y modificados después de hacer en la terminal un__:

```
$ git status
```

__Fue entonces cuando pensé en usar Fzf para filtrar/buscar entre todos estos archivos para los que quería seleccionar y realizar una acción con git (add, checkout, rm, etc)__. Al final programé un script en python que toma la salida estándar del comando **git status** y se la pasa a la entrada estándar de **Fzf** y ¡ vualá !, el script **OMGit-fzf** funciona.

### Qué es OMGit-fzf

Es un script/comando que he programado bajo licencia GPL v3. Que puede descargar y utilizar a su gusto desde su repo OMGit-fzf. Este comando permite desde la terminal ir más rápido con git y sus tareas, lo que hace es que nos permite buscar, filtrar y seleccionar la salida estándar de **git status** y lanzar comandos con un rudimentario menú.

**Screencast explicando el uso de mi script OMGit-fzf y sus opciones:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/wk2vN3zcLH8" frameborder="0" gesture="media" allowfullscreen></iframe>

> Los shorcuts que uso para el desplazamiento dentro de fzf son: ctrl+p y ctrl+n, por aquello de no despegar los dedos de las home keys del teclado.

**Enlaces de referencia:**

- [Pipes](https://codigofacilito.com/articulos/pipes)
- [Entrada estándar y salida estándar](https://es.wikipedia.org/wiki/Entrada_est%C3%A1ndar)
- Repo [Fzf](https://github.com/junegunn/fzf)
- Repo [fzf.vim](https://github.com/junegunn/fzf.vim)
- Otros proyectos que hacen uso del potencial de Fzf, [Proyectos relacionados](https://github.com/junegunn/fzf/wiki/Related-projects)
- Mi repo [OMGit-fzf](https://gitlab.com/Energy1011/omgit)

> OTROS CASOS: He programado otros scripts que hacen uso de Fzf, en los que los scripts conectan a una API mediante protocolo http desarrolladas con laravel y nodejs, en las que las respuestas de estas API pueden ser pasadas a Fzf. Por ejemplo un script que conecta con una API en laravel para lanzar comandos Artisan con modelos del sistema devueltos por métodos de la API, haciendo uso también del comando  'artisan list'. Entonces los usos y las posibilidades son infinitas y hasta donde la creatividad nos lleve.

Aportaciones, sugerencias al código de OMGit-fzf son siempre bienvenidas, para finalizar este post, quiero primero dar las gracias a todas las personas que contribuyen con Fzf y todos los proyectos con licencias libres, no solo programando, también difundiendo y compartiendo software libre.

Espero este script contiene apenas 107 líneas de código les sea tan útil como lo es para mí. Hasta la próxima.
