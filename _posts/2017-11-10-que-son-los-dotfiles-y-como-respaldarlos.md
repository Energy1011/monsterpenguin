---
layout: post
title: 'Qué son los dotfiles y cómo respaldarlos'
date: 2017-11-10 02:00:00 -06:00
type: post
published: true
status: publish
categories: [linux, unix, blog]
tags: [dotfiles, unix]
meta:
author: "Energy1011"
image: "logo-dotfiles.png"

---

Hace poco más un mes que no escribía en el blog, pero aquí estoy de regreso con un tema básico para cualquier *nix-user, los llamados __Dotfiles__ (archivos de configuración ocultos).

### ¿ Qué son los dotfiles ?

En sistemas unix-like, refiriéndonos a los sistemas basados en unix, digamos: GNU/Linux, MAC OS X, FreeBSD, etc. Sistemas en los que si bien recordamos, los archivos ocultos son nombrados comenzando con un punto (.) 'dot' en inglés, los dotfiles son archivos ocultos en nuestro sistema que guardan configuraciones.

### ¿ Dónde encuentro estos archivos y para qué sirven ?

Muchos programas hacen uso de sus propios archivos dotfiles, estos son creados al instalar el programa o ejecutarlo y contienen configuraciones para el funcionamiento de la aplicación, estos dotfiles suelen estar ubicados dentro del la carpeta home del usuario o en carpetas ocultas dentro de home.

Existen archivos dotfiles muy conocidos (.bashrc en Mac OS X llamado: .bash_profile, .gitignore, .vimrc) este último por ejemplo, tenemos que Vim (mi editor favorito) crea y usa un archivo dotfile llamado __.vimrc__ por default para guardar algunas configuraciones para el editor vim. Los nombres y rutas de los archivos dotfile pueden variar y ser modificados en los programas que hacen uso de ellos, aun así es importante mencionar que muchos archivos dotfile tienen el posfijo 'rc' (como .vimrc) o 'config'.

El archivo __.vimrc__ podemos encontrarlo en el directorio home del usuario __~/.vimrc__ en el que __'~/'__ tilde-virgulilla-de-eñe y diagonal se refiere al directorio home del usuario.

Dentro del archivo dotfile de vim __también llamado archivo de configuración de vim__ podemos encontrar algo parecido a esto:
```vim
" URL: http://vim.wikia.com/wiki/Example_vimrc
" Authors: http://vim.wikia.com/wiki/Vim_on_Freenode
" Description: A minimal, but feature rich, example .vimrc. If you are a
"              newbie, basing your first .vimrc on this file is a good choice.
"              If you're a more advanced user, building your own .vimrc based
"              on this file is still a good idea.

"------------------------------------------------------------
" Features
"
" These options and commands enable some very useful features in Vim, that
" no user should have to live without.

" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible

" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on

" Enable syntax highlighting
syntax on
```

### ¿ El contenido de los dotfiles está en el mismo 'lenguaje' universal y estructura ?
Los archivos dotfiles son distintos y dependen del programa que hace uso de ellos, entonces NO utilizan todos el mismo lenguaje o estructura, el contenido de estos archivos puede variar entre programas, es decir, cada programa define la sintaxis y el cómo leer estos archivos. La información que guardan estos archivos son comunmente variables o constantes que definen el comportamiento o configuraciones persistentes para el programa, por ejemplo: añadiendo la siguiente línea al archivo __.vimrc__ hacemos que vim siempre nos muestre los números de línea de un archivo dentro del editor, ya que el editor vim por configuración default nunca muestra los numeros de línea en el editor

```vim
set number
```

o también con :

```vim
set nu
```
### El dotfile .vimrc nos invita a basarnos y hacer adaptaciones
La siguiente es una traducción al español de un comentario/fragmento del archivo .vimrc ya mostrado arriba en este post
```vim
"Descripción: Un ejemplo mínimo, pero rico en características .vimrc. Si eres un
"novato, basar tu primer .vimrc en este archivo es una buena opción.
"Si eres un usuario más avanzado, construye tu propio .vimrc
"en este archivo sigue siendo una buena idea.
```

__Esta descripción promueve a adaptar dotfiles__, y basarnos en configuraciones que han hecho otras personas. Para hacer nuestras propias modificaciones siempre es bueno primero buscar su documentación y mucha de esta información podemos encontrarla a manera de comentario dentro del mismo dotfile.

### Los dotfiles se pueden compartir
Es buena idea basarnos en dotfiles de otras personas, si le agrada la configuración de un amigo para su editor vim por ejemplo, podría pedirle que le comparta su dotfile y hacer sus propias adaptaciones, copiando y ajustando sus propios detalles.

En lo personal suelo utilizar y compartir los siguientes dotfiles, por mencionar algunos:

- bash (~/.bashrc bash shell)
- i3wm (~/.i3/config window manager)
- vim (~/.vimrc editor de texto y código)
- tmux (terminal multiplexer)
- vimfx (un legacy plugin de firefox para navegar en modo vim)

### Respaldando e "instalando" dotfiles
Los dotfiles pueden contener muchas configuraciones específicas del usuario y la cantidad de dotfiles dependerá de la cantidad de programas que el usuario quiera guardar configuraciones y que estos programas por supuesto implementen este tipo de archivos dotfiles.

Para tener nuestros dotfiles resguardados y disponibles en cualquier momento para instalar en cualquier sistema, __un día alguien tuvo la brillante idea de crear un repositorio__ y llevarlo con un sistema de control de versiones como lo es __Git__; Utilizando git entonces podemos guardar las versiones de nuestros dotfiles y tener estos archivos disponibles para clonarlos como cualquier otro repo e instalarlos en una computadora recientemente formateada, en una nueva distro con la que estemos trabajando o en una compuradora prestada en nuestro lugar de trabajo, etc.

__IMPORTANTE__: es una buena práctica de seguridad revisar que los dotfiles que vayamos a compartir no contengan información del tipo passwords o cualquier otro tipo de dato que nos pueda ser confidencial. 

La práctica de resguardar y compatir dotfiles mediante un repo se ha extendido y ha resultado popular y muy útil. 

[Dejo una guía no-oficial de Github que explica y recomienda la creación de repositorios dotfiles (Aquí)](https://dotfiles.github.io/)

### Mi repo dotfile
Compartir configuraciones de dotfiles me parece bueno y puede beneficiarnos a todos. La idea de respaldar e instalar los dotfiles me pareció excelente y de inmediato quise crear mi propio repo y programé dos scripts en bash para facilitarme y automatizar el trabajo para cada que queria dotar a un nuevo sistema con mis propios dotfiles, entonces programé dos scripts: __Uno para crear backups de los dotfiles que me interesan__ y otro __para instalarlos (copiarlos)__ de manera automática en sus respectivos directorios; [Puedes acceder a mi repo dotfiles (Aquí)](https://gitlab.com/Energy1011/dotfiles) 

__Mi repo dotfiles contiene lo siguiente:__

- El script que he creado para respaldo, en el que el script pregunta al usuario que dotfiles respaldar:
[backup.sh](https://gitlab.com/Energy1011/dotfiles/blob/master/backup.sh)

- Para instalar mis dotfiles he creado este otro script que va preguntando al usuario que instalar y que no:
[install.sh](https://gitlab.com/Energy1011/dotfiles/blob/master/install.sh)
(Es posible que la palabra "instalar" no sea la más adecuada, ya que lo que hace este script es solo copiar los dotfiles a sus ubicaciones correctas en el sistema, quizas "Restaurar" sea una palabra más adecuada pero he decidido quedarme con install.sh ya que me parece un termino más extendido)

- Y por supuesto: Los archivos dotfile

__Siéntase libre de clonar mi repo y adaptarlo a su gusto :)__  

Le invito a usar los scripts a sus necesidades o colaborar con un pull request si lo desea, (por lo pronto un amigo me ha recomendado agregarle a los scripts una opción de (yes to all) para las preguntas que nos van guiando en la instalación de los dotfiles y de respaldo).

### Recapitulando esta publicación
- En los sistemas operativos unix-like los archivos son nombrados y comienzan con un (.) 'dot' en inglés.
- Los archivos dotfiles son archivos de configuración ocultos utilizados por distintos programas.
- Estos archivos pueden ser respaldados en repos de git para tenerlos disponibles y hacer uso de ellos desde cualquier sistema.
- Es buena idea aprender/clonar/crear repos dotfiles, más que nada para basarte en otros repositorios y mejorar tus dotfiles.
- Puedes acceder a mi repo dotfiles [(Aquí)](https://gitlab.com/Energy1011/dotfiles) 


A manera general he explicado los dotfiles en esta publicación y espero esta información sea útil, hasta la próxima.
